<h2>Introduction</h2>
Tool to delete pipeline(s) from gitlab, as gitlab lacks a multi-select option for deleting pipelines. At the moment you can only delete a pipeline after selecting it and clicking the delete button.<br>
This tools offers a GUI enabling you to bulk-deleting pipelines based on branch name, result status and/or (a range of) pipeline id's. 
It builds a user interface which retrieves all projects from a given gitlab instance, which can be specified in a properties file.<br>
The required gitlab token can be specified in your maven settings.xml (if you have one on your workstation), or properties file and if not provided in either, the application will ask for it at startup.<br>
Using the provided comboboxes you can select your project and at the bottom half all available pipelines will be shown for the selected project<br>
You can proceed by selecting a branch and/or pipeline status and further narrow down your selection by manually specifying one or more pipeline id('s)<br>

<p>Note: Deleting a pipeline requires owner privileges for the selected project!!</p>

<h2>Requirements</h2>
Install python version >= 3.6<br>
Create a gitlab token.<br>
Install tkinter (Linux: sudo apt-get install python3-tk; Windows: select "tk/tcl and IDLE" option during installation of python; Mac OS: See https://www.python.org/download/mac/tcltk/)<br>
Create a virtual environment, activate it and run: "pip3 install -r requirements.txt"; Note when using pipenv, just run "pipenv install" and "pipenv shell"

<h2>Usage:</h2>
Start in a terminal window via "python3 delete_pipelines.py".<br>
On windows from a command window "python delete_pipelines.py". 
On windows don't forget to add your python's installation bin folder to the system PATH environment variable.<br>
Specify your gitlab url in the properties file "delete_pipelines.properties" and token in either your maven settings.xml ([homefolder/.m2/settings.xml]), properties file or on the command line at startup when asked for it.<br>
The token will first be searched for in your maven settings.xml file. If not found it will be taken from your properties file. If it is not specified in either location, it will be asked for at application startup.<br>
You can change the labels of every user interface element in the property file.<br>
The property file also provides a key "namespaceFilter", which can be used to only show the projects in the namespaces provided in this key (comma separated).
When left empty or not provided no filtering takes place.<br>

<h3>Stubbing</h3>
The property file specifies a key "stubbed". When set to "True" a set of provided json exports - available in folder json - will be used to simulate/stub an 
actual gitlab instance.<br>
This may be useful when you start using this tool to see how the tool works without actually modifying your live gitlab projects, 
or when you want to modify it after cloning and testing your changes safely.
You can use git diff to see what happens when you delete one or more pipelines, because when a pipeline is deleted the json export will be updated.
A simple revert takes you back to your original stubbed project and dummy pipeline data.

