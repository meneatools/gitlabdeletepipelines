"""
GUI helper methods
"""
from tkinter import TOP, ttk
from tkinter.ttk import Progressbar

from util.util import format_pipelines, PIPELINE_INDEX


def repopulate_projects(project_path, all_project_paths, combo_project_names, combo_branches, combo_status):
    if project_path == ''or project_path not in all_project_paths.keys():
        return

    projects = all_project_paths[project_path]
    if projects is None or len(projects) == 0:
        return

    items = [project['name'] for project in projects]
    sorted_project_names = sorted(items)
    combo_project_names.set('')
    combo_project_names['values'] = tuple(sorted_project_names)
    combo_project_names.focus()
    combo_branches.set('')
    combo_status.set('')


def repopulate_branches(project_pipelines, combo_branches, status):
    if status == "":
        branches = {project_pipeline[PIPELINE_INDEX.BRANCH] for project_pipeline in project_pipelines}
        combo_branches.set('')
    else:
        branches = {project_pipeline[PIPELINE_INDEX.BRANCH] for project_pipeline in project_pipelines
                    if project_pipeline[PIPELINE_INDEX.STATUS] == status}
        if combo_branches.get() == "" or combo_branches.get() not in branches:
            combo_branches.set('')

    sorted_branches = sorted(branches, key=lambda branch: branch.lower())
    combo_branches['values'] = tuple(sorted_branches)


def repopulate_statusses(project_pipelines, combo_status, branch):
    if branch == "":
        statusses = {project_pipeline[PIPELINE_INDEX.STATUS] for project_pipeline in project_pipelines}
        combo_status.set('')
    else:
        statusses = {project_pipeline[PIPELINE_INDEX.STATUS] for project_pipeline in project_pipelines
                     if project_pipeline[PIPELINE_INDEX.BRANCH] == branch}
        if combo_status.get() == "" or combo_status.get() not in statusses:
            combo_status.set('')

    sorted_statusses = sorted(statusses)
    combo_status['values'] = tuple(sorted_statusses)


def get_pipelines_for_branch_and_status(project_pipelines, selected_branch, selected_status):
    pipelines_for_branch_and_status = []
    if selected_branch == "" and selected_status == "":
        pipelines_for_branch_and_status = project_pipelines
    else:
        for item in project_pipelines:
            branch = item[PIPELINE_INDEX.BRANCH]
            status = item[PIPELINE_INDEX.STATUS]
            if selected_branch != "" and selected_status != "":
                if branch == selected_branch and status == selected_status:
                    pipelines_for_branch_and_status.append(item)
            elif (selected_branch != "" and branch == selected_branch) or (
                    selected_status != "" and status == selected_status):
                pipelines_for_branch_and_status.append(item)

    return pipelines_for_branch_and_status


def get_content_available_pipelines(project_name, pipelines):
    return f"Number of pipelines for project [{project_name}]: [{len(pipelines)}]\n{format_pipelines(pipelines)}"


def create_progress_bar(root, maximum):
    style = ttk.Style(root)
    style.layout('text.Horizontal.TProgressbar',
                 [('Horizontal.Progressbar.trough',
                   {'children': [('Horizontal.Progressbar.pbar',
                                  {'side': 'left', 'sticky': 'ns'})],
                    'sticky': 'nswe'}),
                  ('Horizontal.Progressbar.label', {'sticky': ''})])  # ,lightcolor=None,bordercolo=None,darkcolor=None
    style.configure('text.Horizontal.TProgressbar', text='0 %')

    progress_bar = Progressbar(root, style='text.Horizontal.TProgressbar', orient="horizontal", length=500,
                               maximum=maximum, value=0, mode='determinate', takefocus=True)
    progress_bar.pack(side=TOP)
    return progress_bar, style
