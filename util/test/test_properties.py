import os
import unittest
import util.properties as PropertiesClass

class TestProperties(unittest.TestCase):

    def setUp(self) -> None:
        testdata = ["key1=value1\n",
                    " key2 = value2 \n",
                    "# some comment\n",
                    "\n",
                    "    \n",
                    "key3=value 3\n",
                    "key4=value4\n",
                    " key5 = value5 \n",
                    " key6 = value 6  \n",
                    "key7= \" \"\n",
                    "key8= \"a b\" "]
        with open("./testme.properties", "w") as file_pointer:
            file_pointer.writelines(testdata)
        self.properties = PropertiesClass.Properties("./testme.properties")

    def test_get_property(self):
        self.assertEqual(self.properties.get_property("key1"), "value1")
        self.assertEqual(self.properties.get_property("key2"), "value2")
        self.assertEqual(self.properties.get_property("key3"), "value 3")
        self.assertEqual(self.properties.get_property("key4"), "value4")
        self.assertEqual(self.properties.get_property("key5"), "value5")
        self.assertEqual(self.properties.get_property("key6"), "value 6")
        self.assertEqual(self.properties.get_property("key7"), " ")
        self.assertEqual(self.properties.get_property("key8"), "a b")
        self.assertEqual(self.properties.get_property("key9", "v9"), "v9")


    def tearDown(self) -> None:
        os.remove("./testme.properties")

if __name__ == '__main__':
    unittest.main()
