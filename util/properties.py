"""
Properties file methods
"""
import os


# pylint: disable=too-few-public-methods
class Properties:
    def __init__(self, file):
        self.file = file
        if os.path.exists(self.file):
            with open(self.file) as file_pointer:
                self._data = file_pointer.readlines()
                self._properties = self._read_properties()

    def get_property(self, key=None, default=""):
        if len(self._properties) == 0:
            return default
        if key in self._properties:
            if key != "certificates":
                return self._properties[key]
            return self._properties["base_folder"] + self._properties["certificates"]
        return default

    def set_property(self, key, value):
        self._properties[key] = value

    def _read_properties(self):
        chars = "=:"
        properties = {}
        for line in self._data:
            line = line.strip()
            if line.startswith("#") or line == "":
                continue
            for char in chars:
                if char in line:
                    elements = line.split(char)
                    key, value = [element.strip() for element in elements]
                    if value.startswith('"') and value.endswith('"'):
                        value = value[1:-1]
                    properties[key] = value
                    break
        return properties


if __name__ == "__main__":
    testdata = ["key1=value1\n",
                " key2 = value2 \n",
                "# some comment\n",
                "\n",
                "    \n",
                "key3=value 3\n",
                "key4=value4\n",
                " key5 = value5 \n",
                " key6 = value 6  \n",
                "key7= \" \"\n",
                "key8= \"a b\" "]
    with open("./test.properties", "w") as file_pointer_test:
        file_pointer_test.writelines(testdata)
    myprop = Properties("./test.properties")
    print(f'[{myprop.get_property(key="key1", default="v1")}]')
    print(f'[{myprop.get_property(key="key2", default="v2")}]')
    print(f'[{myprop.get_property(key="key3", default="v3")}]')
    print(f'[{myprop.get_property(key="key4", default="v4")}]')
    print(f'[{myprop.get_property(key="key5", default="v5")}]')
    print(f'[{myprop.get_property(key="key6", default="v6")}]')
    print(f'[{myprop.get_property(key="key7", default="v7")}]')
    print(f'[{myprop.get_property(key="key8", default="v8")}]')
    print(f'[{myprop.get_property(key="key9", default="v9")}]')
