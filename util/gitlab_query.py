"""
gitlab methods
"""
import json
import requests

from util.gui_helper import create_progress_bar

KEY_GITLAB_URL = "gitlabUrl"
KEY_USERS = "users"
KEY_PROJECT_GROUPS = "projectGroups"
KEY_NAMESPACE_FILTER = "namespaceFilter"
KEY_CERTIFICATES = "certificates"


def get_without_headers(base_url, certificates):
    if certificates == "":
        return requests.get(base_url)
    elif certificates == "False":
        return requests.get(base_url, verify=False)
    return requests.get(base_url, verify=certificates)


def get(url, certificates, headers=None):
    if headers is None:
        return get_without_headers(url, certificates)

    if certificates == "":
        return requests.get(url, headers=headers)
    elif certificates == "False":
        return requests.get(url, headers=headers, verify=False)
    return requests.get(url, headers=headers, verify=certificates)


def delete(url, certificates, headers):
    if certificates == "":
        return requests.delete(url, headers=headers)
    elif certificates == "False":
        return requests.delete(url, headers=headers, verify=False)
    return requests.delete(url, headers=headers, verify=certificates)


# pylint: disable=too-many-locals
def get_all_projects(token, properties):
    gitlab_url = properties.get_property(KEY_GITLAB_URL, "")
    if gitlab_url == "":
        return ""

    users = properties.get_property(KEY_USERS)
    if users == "":
        base_urls = [f"{gitlab_url}/api/v4/projects?owned=yes&per_page=100"]
    else:
        base_urls = []
        list_users = [user.strip() for user in users.split(",")]
        for user in list_users:
            base_urls.append(f"{gitlab_url}/api/v4/users/{user}/projects?owned=yes&per_page=100")

    certificates = properties.get_property(KEY_CERTIFICATES, "")

    namespace_filter = get_namespace_filter(properties)
    headers = { 'PRIVATE-TOKEN': token }
    all_projects = []
    for base_url in base_urls:
        page_of_projects, response = append_projects_on_page(base_url, headers, namespace_filter, certificates)
        if response.status_code != 200:
            continue
        all_projects.extend(page_of_projects)
        page_no = 1
        next_page = response.headers.get("x-next-page")
        while next_page != "":
            page_no = page_no + 1
            print(f"Fetching page [{page_no}] ...")
            url = f"{base_url}&page={page_no}"
            page_of_projects, response = append_projects_on_page(url, headers, namespace_filter, certificates)
            all_projects.extend(page_of_projects)
            next_page = response.headers.get("x-next-page")

# temporary for building json files for stubbing
#     KEY_ALL_PROJECTS = "all_projects"
#     jsonFile = properties.getProperty(KEY_ALL_PROJECTS)
#     with open(jsonFile, 'w') as file_pointer:
#          file_pointer.write(json.dumps(all_projects))

    return all_projects


def get_namespace_filter(properties):
    namespace_filter = properties.get_property(KEY_NAMESPACE_FILTER, "")
    if namespace_filter == "":
        return []
    return [namespace.strip() for namespace in namespace_filter.split(",")]


def append_projects_on_page(base_url, headers, namespace_filter, certificates):
    response = get(base_url, certificates, headers=headers)
    result = response.content
    projects = json.loads(result)
    all_projects = []
    for project in projects:
        name = project['name']
        namespace_fullpath = project['namespace']['full_path']
        namespace_name = project['namespace']['name']
        namespace_path = project['namespace']['path']
        project_id = "{:03d}".format(project['id'])

        if len(namespace_filter) == 0 or namespace_fullpath in namespace_filter:
            all_projects.append({'projectName': name,
                                 'namespace_fullpath': namespace_fullpath,
                                 'projectId': project_id,
                                 'namespace_name': namespace_name,
                                 'namespace_path': namespace_path})

    return all_projects, response


def get_all_pipelines_for_project(project_id, token, properties):
    gitlab_url = properties.get_property(KEY_GITLAB_URL)
    base_url = f"{gitlab_url}/api/v4/projects/{project_id}/pipelines?private_token={token}&per_page=100"
    certificates = properties.get_property(KEY_CERTIFICATES, "")
    response = get(base_url, certificates)
    result = response.content
    pipelines = json.loads(result)
    pipeline_ids = []
    add_pipeline_ids(pipelines, pipeline_ids)
    next_page = response.headers.get("x-next-page")
    index = 1
    while next_page != "":
        index = index + 1
        url = f"{base_url}&page={index}"
        response = get(url, certificates)
        result = response.content
        pipelines = json.loads(result)
        add_pipeline_ids(pipelines, pipeline_ids)
        next_page = response.headers.get("x-next-page")

# temporary
#     KEY_PIPELINES_DUMP = "pipelinesDump"
#     jsonFile = properties.getProperty(KEY_PIPELINES_DUMP)
#     jsonFileProject = jsonFile.format(projectId)
#     with open(jsonFileProject, 'w') as file_pointer:
#          file_pointer.write(json.dumps(pipelineIds))

    return pipeline_ids


def add_pipeline_ids(pipelines, pipeline_ids):
    for pipeline in pipelines:
        try:
            pipeline_ids.append((pipeline['id'], pipeline['created_at'], pipeline['ref'], pipeline['status']))
        except TypeError as exc:
            print(f"Failed to add pipeline {pipeline}, error [{exc}]")


def delete_pipelines(project_id, pipelines_to_delete, properties, root, token):
    gitlab_url = properties.get_property(KEY_GITLAB_URL)
    result = 204
    done = 0
    failed = []
    number_of_pipelines_to_delete = len(pipelines_to_delete)
    progress_bar, style = create_progress_bar(root, number_of_pipelines_to_delete)
    base_url = "{}/api/v4/projects/{}/pipelines/{}"
    for pipeline in pipelines_to_delete:
        done = done + 1
        url = base_url.format(gitlab_url, project_id, pipeline)
        headers = { 'PRIVATE-TOKEN': token }
        certificates = properties.get_property(KEY_CERTIFICATES, "")
        response = delete(url, certificates, headers=headers)
        if response.status_code != 204:
            # failed to delete pipeline
            failed.append(pipeline)
            result = response.status_code
        progress_bar['value'] = done
        progress = done / number_of_pipelines_to_delete
        style.configure('text.Horizontal.TProgressbar',
                        text=f'{done}/{number_of_pipelines_to_delete} completed - {100 * progress:.2f} %')
        root.update_idletasks()
        root.update()

    progress_bar.destroy()
    return result, failed
