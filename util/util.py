import os
from pathlib import Path


class PIPELINE_INDEX:
    ID, CREATED_AT, BRANCH, STATUS = range(0, 4)


def get_token(lines):
    for index, line in enumerate(lines):
        if "<name>Private-Token</name>" in line:
            token_line = lines[index+1]
            start = token_line.index("<value>")+len("<value>")
            end = token_line.index("</value>")
            return token_line[start:end]
    return ""


def get_token_from_maven_settings_xml():
    home = str(Path.home())
    mvn_settings_file = os.path.join(home, ".m2", "settings.xml")
    try:
        with open(mvn_settings_file) as file_pointer:
            lines = file_pointer.readlines()
            token = get_token(lines)
        return token
    except:
        print(f"Maven settings file [{mvn_settings_file}] not found!")
        return ""


def format_pipelines(project_pipelines):
    formatted = ""
    for index, project_pipeline in enumerate(project_pipelines):
        id = project_pipeline[PIPELINE_INDEX.ID]
        created_at = project_pipeline[PIPELINE_INDEX.CREATED_AT]
        branch = project_pipeline[PIPELINE_INDEX.BRANCH]
        status = project_pipeline[PIPELINE_INDEX.STATUS]
        formatted = f"{formatted}\n{id} ({created_at}, {status:8}, {branch})"
    return formatted


def get_pipelines(pipelines_to_delete):
    if pipelines_to_delete == "":
        return []

    if "," in pipelines_to_delete:
        pipelines = [int(pipeline) for pipeline in pipelines_to_delete.split(",")]
    else:
        if "-" in pipelines_to_delete:
            pipeline_boundaries = pipelines_to_delete.split("-")
        else:
            pipeline_boundaries = [pipelines_to_delete, pipelines_to_delete]
        pipelines = range(int(pipeline_boundaries[0]), int(pipeline_boundaries[1]) + 1)
    return pipelines


def intersection(list1, list2):
    return list(set(list1) & set(list2))


def get_final_pipelines_to_delete(project_pipelines, pipelines_to_delete="", branch="", status=""):
    pipelines_to_delete_list = get_pipelines(pipelines_to_delete)

    branch_pipeline_ids = []
    status_pipeline_ids = []
    pipelines_to_delete_ids = []
    for pipeline in project_pipelines:
        (p_id, p_created, p_branch, p_status) = pipeline
        if branch == p_branch:
            branch_pipeline_ids.append(p_id)
        if status == p_status:
            status_pipeline_ids.append(p_id)
        if p_id in pipelines_to_delete_list:
            pipelines_to_delete_ids.append(p_id)

    if len(pipelines_to_delete_ids) > 0 and len(branch_pipeline_ids) > 0:
        intersection_list = intersection(pipelines_to_delete_ids, branch_pipeline_ids)
        if len(status_pipeline_ids) > 0:
            return intersection(status_pipeline_ids, intersection_list)
        else:
            return intersection_list
    elif len(branch_pipeline_ids) > 0:
        if len(status_pipeline_ids) > 0:
            return intersection(status_pipeline_ids, branch_pipeline_ids)
        else:
            return branch_pipeline_ids
    elif len(status_pipeline_ids) > 0:
        if len(pipelines_to_delete_ids) > 0:
            return intersection(status_pipeline_ids, pipelines_to_delete_ids)
        else:
            return status_pipeline_ids
    else:
        return pipelines_to_delete_ids


def get_project_id(project_path, project_name, projects_for_path):
    projects = projects_for_path[project_path]
    if projects is None or len(projects) == 0:
        return None

    for project in projects:
        if project['name'] == project_name:
            return project['id']

    return None


def get_projects_for_paths(all_projects):
    projects_for_path = {}
    for project in all_projects:
        name = project['projectName']
        path = project['namespace_fullpath']
        id = project['projectId']
        if path not in projects_for_path.keys():
            projects = [{'name':name, 'id':id}]
        else:
            projects = projects_for_path[path]
            projects.append({'name':name, 'id':id})
        projects_for_path[path] = projects
    return projects_for_path
