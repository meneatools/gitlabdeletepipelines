"""
gitlab stub methods
"""
import json
import os
from typing import List, Dict, Optional, Tuple

from util.gui_helper import create_progress_bar
from util.properties import Properties

KEY_PIPELINES_DUMP = "pipelinesDump"
KEY_ALL_PROJECTS = "all_projects"
KEY_PROJECT_GROUPS = "projectGroups"
KEY_NAMESPACE_FILTER = "namespaceFilterStubs"

# pylint: disable=unused-argument
def get_all_projects(token: str, properties: Properties) -> List[Dict]:
    """
    get all projects from the stubbed json files


    :param token: gitlab token, in here for consistency with 'real' gitlab module
    :param properties:
    :return:
    """
    namespace_filter = get_namespace_filter(properties)
    allprojects_json_file = properties.get_property(KEY_ALL_PROJECTS)
    with open(allprojects_json_file) as file_pointer:
        content = file_pointer.read()
        projects_json = json.loads(content)

    all_projects = []
    for project in projects_json:
        name = project['projectName']
        namespace_fullpath = project['namespace_fullpath']
        namespace_name = project['namespace_name']
        namespace_path = project['namespace_path']
        project_id = project['projectId']

        if len(namespace_filter) == 0 or namespace_fullpath in namespace_filter:
            all_projects.append({'projectName':name,
                                'namespace_fullpath':namespace_fullpath,
                                'projectId':project_id,
                                'namespace_name':namespace_name,
                                'namespace_path':namespace_path})
    return all_projects


def get_namespace_filter(properties: Properties) -> List[str]:
    """
    :param properties:
    :return: list of namespaces based on filter
    """
    namespace_filter = properties.get_property(KEY_NAMESPACE_FILTER, "")
    if namespace_filter == "":
        return []
    return [namespace.strip() for namespace in namespace_filter.split(",")]


def get_all_pipelines_for_project(project_id: str, token: str, properties: Properties) -> List[Optional[List[Dict]]]:
    """

    :param project_id:
    :param token:
    :param properties:
    :return: list of pipelines for given project
    """
    json_file = properties.get_property(KEY_PIPELINES_DUMP).format(project_id)
    if not os.path.exists(json_file):
        return []

    with open(json_file) as file_pointer:
        content = file_pointer.read()
        pipelines_json = json.loads(content)
    return pipelines_json

# pylint: disable=too-many-locals
def delete_pipelines(project_id: str, pipelines_to_delete: list, properties: Properties, root, token: str) -> \
                        Tuple[int, None]:
    """

    :param project_id:
    :param pipelines_to_delete:
    :param properties:
    :param root:
    :param token:
    :return: status code (204, 404) and None
    """
    json_file = properties.get_property(KEY_PIPELINES_DUMP).format(project_id)
    if not os.path.exists(json_file):
        return 404, None

    with open(json_file) as file_pointer:
        content = file_pointer.read()
        pipelines_json = json.loads(content)
        pipeline_ids = [pipeline[0] for pipeline in pipelines_json]

    any_pipelines_deleted = False
    number_of_pipelines_to_delete = len(pipelines_to_delete)
    progress_bar, style = create_progress_bar(root, number_of_pipelines_to_delete)
    done = 0
    for pipeline_id in pipelines_to_delete:
        done = done + 1
        try:
            index = pipeline_ids.index(pipeline_id)
            del pipelines_json[index]
            del pipeline_ids[index]
            any_pipelines_deleted = True
        except ValueError:
            # pipeline not found, so nothing to do ...
            pass
        progress_bar['value'] = done
        progress = done / number_of_pipelines_to_delete
        style.configure('text.Horizontal.TProgressbar', text=f'{done} completed - {100 * progress:.2f} %')
        root.update_idletasks()
        root.update()

    progress_bar.destroy()

    if any_pipelines_deleted:
        with open(json_file, 'w') as file_pointer:
            file_pointer.write(json.dumps(pipelines_json))
        return 204, None
    return 404, None
