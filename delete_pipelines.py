"""
main logic for building gui and deleting gitlab pipelines via gui
"""
import importlib
import os
import sys
from getpass import getpass
from tkinter import INSERT, Label, Frame, Tk, Entry, END, E, W, WORD, messagebox, BOTTOM, Button, X
from tkinter.scrolledtext import ScrolledText
from tkinter.ttk import Combobox
import tkinter as tk

from util.properties import Properties
from util.gui_helper import repopulate_projects, repopulate_branches, repopulate_statusses, \
    get_pipelines_for_branch_and_status, get_content_available_pipelines
from util.util import get_final_pipelines_to_delete, get_token_from_maven_settings_xml, get_project_id, \
    get_projects_for_paths

KEY_WINDOW_TITLE = "windowTitle"
KEY_GITLAB_TOKEN = "gitlabToken"
KEY_LABEL_PROJECT_PATH_COMBO = "labelProjectPathCombo"
KEY_LABEL_PROJECT_NAME_COMBO = "labelProjectNameCombo"
KEY_LABEL_BRANCH_COMBO = "labelBranchCombo"
KEY_LABEL_STATUS_COMBO = "labelStatusCombo"
KEY_LABEL_PIPELINES_ENTRY = "labelPipelinesEntry"
KEY_LABEL_AVAILABLE_PIPELINES_SCROLLED_TEXT = "labelAvailablePipelinesScrolledText"
KEY_LABEL_EXECUTE_BUTTON = "labelExecuteButton"
KEY_LABEL_QUIT_BUTTON = "labelQuitButton"
KEY_MSGBOX_DELETE_PIPELINES_TITLE = "messageBoxDeletePipelinesTitle"
KEY_MSGBOX_QUIT_TITLE = "messageBoxQuitTitle"
KEY_MSGBOX_QUIT_MESSAGE = "messageBoxQuitMessage"
KEY_STUBBED = "stubbed"


# ===========================================================================
# Build the user-interface
# ===========================================================================
# pylint: disable=too-many-instance-attributes,too-many-locals,too-many-statements,attribute-defined-outside-init
class BuildGui():

    def __init__(self, all_projects, token, properties, gitlab_query_module):
        self.all_project_paths = get_projects_for_paths(all_projects)
        max_width = max(max(len(project['projectName']) for project in all_projects),
                        max(len(project_path) for project_path in self.all_project_paths.keys()))
        self.max_width = max(20, max_width)
        self.project_path_values = sorted(list(self.all_project_paths.keys()))
        self.token = token
        self.properties = properties
        self.gitlab_query_module = gitlab_query_module
        self.bg_color = 'light grey'

    def build(self):
        self.root = Tk()
        window_title = self.properties.get_property(KEY_WINDOW_TITLE)
        self.root.title(window_title)
        self.root.configure(background=self.bg_color)
        self.root.iconphoto(False, tk.PhotoImage(file=f'{os.path.dirname(os.path.realpath(__file__))}/app.png'))

        # ===================
        # Add a grid
        # ===================
        self.mainframe = Frame(self.root)
        self.mainframe.configure(background=self.bg_color)
        # self.mainframe.grid(row = 0, columnspan=2)
        self.mainframe.columnconfigure(0, pad = 3)
        self.mainframe.columnconfigure(1, pad = 3)
        for i in range(0, 5):
            self.mainframe.rowconfigure(i, pad = 3)
        # distance of the frame from the edges of the window
        self.mainframe.pack(pady = 20, padx = 20)

        # ===================
        # initialize gui elements, configuration takes place further down
        # ===================
        self.combo_project_names = Combobox(self.mainframe, values=(), width=self.max_width)
        self.combo_branches = Combobox(self.mainframe, values=(), width=self.max_width)
        self.combo_status = Combobox(self.mainframe, values=(), width=self.max_width)
        self.pipelines_entry = Entry(self.mainframe, bg='white')
        self.available_project_pipelines = ScrolledText(self.mainframe, wrap=WORD, bg=self.bg_color)
        self.combo_project_path = Combobox(self.mainframe, values=(self.project_path_values), width=self.max_width)

        # ===================
        # labels
        # ===================
        label_project_path_combo = self.properties.get_property(KEY_LABEL_PROJECT_PATH_COMBO, "Select a project path:")
        label_project_name_combo = self.properties.get_property(KEY_LABEL_PROJECT_NAME_COMBO, "Select a project")
        label_branch_combo = self.properties.get_property(KEY_LABEL_BRANCH_COMBO,
                                                          "Delete pipeline for selected branch (optional):")
        label_status_combo = self.properties.get_property(KEY_LABEL_STATUS_COMBO,
                                                          "Delete pipeline for selected status (optional):")
        label_pipelines_entry = self.properties.get_property(KEY_LABEL_PIPELINES_ENTRY,
                                                             "Delete pipeline(s) - '<n>' or '<x>-<y>' or "
                                                             "'<x>,<y>,<z>,...' (optional):")
        label_available_pipelines_scrolled_text = \
            self.properties.get_property(KEY_LABEL_AVAILABLE_PIPELINES_SCROLLED_TEXT,
                                         "Pipelines for project [id (created at, status, branch)]:")
        label_execute_button = self.properties.get_property(KEY_LABEL_EXECUTE_BUTTON, "Execute")
        label_quit_button = self.properties.get_property(KEY_LABEL_QUIT_BUTTON, "Quit")
        message_box_delete_pipelines_title = self.properties.get_property(KEY_MSGBOX_DELETE_PIPELINES_TITLE,
                                                                          "Delete pipelines")
        message_box_quit_title = self.properties.get_property(KEY_MSGBOX_QUIT_TITLE, "Exit Application")
        message_box_quit_message = self.properties.get_property(KEY_MSGBOX_QUIT_MESSAGE,
                                                                "Are you sure you want to quit?")

        row = 0

        # ============================================================
        # Project names : initialize for usage in repopulateProjects in project types and project groups
        # ============================================================
        self.combo_project_names.set('')

        # ============================================================
        # Project paths
        # ============================================================
        row += 1
        Label(self.mainframe, text=label_project_path_combo, bg=self.bg_color).grid(row=row, column=0, sticky=W)
        self.combo_project_path.grid(row=row, column=1, sticky=E)

        def change_project_path(event=None):
            self.available_project_pipelines.delete('1.0', END)
            self.combo_branches['values'] = ()
            self.combo_status['values'] = ()
            if event:
                repopulate_projects(event.widget.get(), self.all_project_paths, self.combo_project_names,
                                    self.combo_branches, self.combo_status)

        # define callback
        self.combo_project_path.bind('<<ComboboxSelected>>', change_project_path)

        # ============================================================
        # Project names (in the context of the project types and groups)
        # ============================================================
        row += 1
        Label(self.mainframe, text=label_project_name_combo, bg=self.bg_color).grid(row=row, column=0, sticky=W)
        self.combo_project_names.grid(row=row, column=1, sticky=E)
        repopulate_projects(self.combo_project_path.get(), self.all_project_paths, self.combo_project_names,
                            self.combo_branches, self.combo_status)

        def change_project_name(event=None):
            self.available_project_pipelines.delete('1.0', END)
            self.combo_branches['values'] = ()
            self.combo_status['values'] = ()
            if event and event.widget.get() != '':
                # get project id
                project_id = get_project_id(project_path=self.combo_project_path.get(), project_name=event.widget.get(),
                                            projects_for_path=self.all_project_paths)

                # get all pipelines for project
                self.project_pipelines = self.gitlab_query_module.get_all_pipelines_for_project(project_id, self.token,
                                                                                                self.properties)
                self.available_project_pipelines.insert(INSERT,
                                                        get_content_available_pipelines(self.combo_project_names.get(),
                                                                                        self.project_pipelines))

                # show branches for which there are pipelines for this project in a combobox
                repopulate_branches(self.project_pipelines, self.combo_branches, self.combo_status.get())

                # show statusses for which there are pipelines for this project in a combobox
                repopulate_statusses(self.project_pipelines, self.combo_status, self.combo_branches.get())

                self.combo_branches.set('')
                self.combo_status.set('')


        # define callback
        self.combo_project_names.bind('<<ComboboxSelected>>', change_project_name)

        # ============================================================
        # Branches
        # ============================================================
        row += 1
        Label(self.mainframe, text=label_branch_combo, bg=self.bg_color).grid(row=row, column=0, sticky=W)
        self.combo_branches.grid(row=row, column=1, sticky=E)

        def change_branch(event=None):
            selected_branch = event.widget.get()

            # show statusses for which there are pipelines for this project in a combobox
            repopulate_statusses(self.project_pipelines, self.combo_status, selected_branch)

            pipelines_for_branch_and_status = get_pipelines_for_branch_and_status(self.project_pipelines,
                                                                                  selected_branch,
                                                                                  self.combo_status.get())

            self.available_project_pipelines.delete('1.0', END)
            self.available_project_pipelines.insert(INSERT,
                                                    get_content_available_pipelines(self.combo_project_names.get(),
                                                                                    pipelines_for_branch_and_status))


        # define callback
        self.combo_branches.bind('<<ComboboxSelected>>', change_branch)


        # ============================================================
        # Status
        # ============================================================
        row += 1
        Label(self.mainframe, text=label_status_combo, bg=self.bg_color).grid(row=row, column=0, sticky=W)
        self.combo_status.grid(row=row, column=1, sticky=E)

        def change_status(event=None):
            selected_status = event.widget.get()

            # show branches for which there are pipelines for this project in a combobox
            repopulate_branches(self.project_pipelines, self.combo_branches, selected_status)

            pipelines_for_branch_and_status = get_pipelines_for_branch_and_status(self.project_pipelines,
                                                                                  self.combo_branches.get(),
                                                                                  selected_status)

            self.available_project_pipelines.delete('1.0', END)
            self.available_project_pipelines.insert(INSERT,
                                                    get_content_available_pipelines(self.combo_project_names.get(),
                                                                                    pipelines_for_branch_and_status))

        # define callback
        self.combo_status.bind('<<ComboboxSelected>>', change_status)


        # ============================================================
        # Pipelines
        # ============================================================
        row += 1
        Label(self.mainframe, text=label_pipelines_entry, bg=self.bg_color).grid(row=row, column=0, sticky=W)
        self.pipelines_entry.grid(row=row, column=1, sticky=E)
        self.pipelines_entry.configure(width=self.max_width + self.combo_status.winfo_width())

        # ============================================================
        # Pipelines for project: label
        # ============================================================
        row += 1
        Label(self.mainframe, text=label_available_pipelines_scrolled_text, bg=self.bg_color)\
            .grid(row=row, column=0, columnspan=2, sticky=W)

        # ============================================================
        # Show (number of) pipelines for the current project
        # ============================================================
        row += 1
        self.available_project_pipelines.grid(row=row, columnspan=2, sticky=(W, E))
        self.available_project_pipelines.insert(INSERT, "")

        # ============================================================
        # Execute removal of selected pipelines for selected projects
        # ============================================================
        def execute():
            self.set_button_state("disabled")
            pipelines_to_delete = self.pipelines_entry.get()
            project_name = self.combo_project_names.get()
            if (pipelines_to_delete == "" and self.combo_branches.get() == '' and self.combo_status.get() == '') \
                    or project_name == "":
                messagebox.showinfo(message_box_delete_pipelines_title,
                                    f"No project [{project_name}] or pipelines [{pipelines_to_delete}] or "
                                    f"branch [{self.combo_branches.get()}] or status [{self.combo_status.get()}] "
                                    f"selected or specified, no action")
            else:
                # get project id
                project_id = get_project_id(project_path=self.combo_project_path.get(),
                                            project_name=self.combo_project_names.get(),
                                            projects_for_path=self.all_project_paths)

                number_of_project_pipelines_before = len(self.project_pipelines)
                final_pipelines_to_delete = get_final_pipelines_to_delete(self.project_pipelines, pipelines_to_delete,
                                                                          self.combo_branches.get(),
                                                                          self.combo_status.get())
                if len(final_pipelines_to_delete) == 0:
                    messagebox.showinfo(message_box_delete_pipelines_title,
                                        "Provided selections yield no non-empty result set of pipeline ids to delete",
                                        parent=self.root)
                    self.set_button_state("normal")
                    return

                # delete requested pipelines
                result, failed = self.gitlab_query_module.delete_pipelines(project_id, final_pipelines_to_delete,
                                                                       properties=self.properties, root=self.root,
                                                                       token=self.token)
                if result != 204:
                    messagebox.showinfo(message_box_delete_pipelines_title,
                                        f"Failed to delete pipelines [{failed}] from project [{project_name}], "
                                        f"error [{result}]",
                                        parent=self.root)
                else:
                    # update textbox with pipelines for project
                    self.project_pipelines = self.gitlab_query_module.get_all_pipelines_for_project(project_id,
                                                                                                    self.token,
                                                                                                    self.properties)
                    number_of_project_pipelines_after = len(self.project_pipelines)
                    diff = number_of_project_pipelines_before - number_of_project_pipelines_after
                    messagebox.showinfo(message_box_delete_pipelines_title,
                                        f"Deleted {diff} pipelines from project [{project_name}]",
                                        parent=self.root)
                    self.available_project_pipelines.delete('1.0', END)
                    self.available_project_pipelines\
                        .insert(INSERT,
                                get_content_available_pipelines(self.combo_project_names.get(),
                                                                self.project_pipelines))

                    # show branches for which there are pipelines for this project in a combobox
                    repopulate_branches(self.project_pipelines, self.combo_branches, "")

                    # show statusses for which there are pipelines for this project in a combobox
                    repopulate_statusses(self.project_pipelines, self.combo_status, "")

                # clear textbox
                self.pipelines_entry.delete(0, END)
                self.set_button_state("normal")

        def quit_gui():
            msgbox = messagebox.askquestion(message_box_quit_title, message_box_quit_message, parent=self.root,
                                            icon='warning')
            if msgbox == 'yes':
                self.root.destroy()

        self.button_frame = Frame(self.root)
        self.button_frame.configure(background=self.bg_color)
        self.button_frame.pack(fill=X, side=BOTTOM)
        self.execute_button = Button(self.button_frame, text=label_execute_button, command=execute,
                                     highlightbackground=self.bg_color)
        self.quit_button = Button(self.button_frame, text=label_quit_button, command=quit_gui,
                                  highlightbackground=self.bg_color)
        self.button_frame.columnconfigure(0, weight = 1)
        self.button_frame.columnconfigure(1, weight = 1)
        self.execute_button.grid(row=0, column=0, sticky=W+E)
        self.quit_button.grid(row=0, column=1, sticky=W+E)

    def set_button_state(self, state):
        self.execute_button["state"] = state
        self.quit_button["state"] = state


# =================================================================
# end of class
# =================================================================
def init():
    properties = Properties(__file__.replace(".py", ".properties"))
    file_path = os.path.realpath(__file__)
    dir_path = os.path.dirname(file_path)
    properties.set_property("base_folder", dir_path)
    token = get_token(properties)
    stubbed = properties.get_property(KEY_STUBBED, "False")
    if stubbed.lower() == "false":
        gitlab_query_module = importlib.import_module('util.gitlab_query')
    else:
        gitlab_query_module = importlib.import_module('stub.gitlab_query')
    projects = gitlab_query_module.get_all_projects(token=token, properties=properties)
    return projects, token, properties, gitlab_query_module


def get_token(properties):
    token = properties.get_property(KEY_GITLAB_TOKEN, "")
    if token != "":
        return token

    token = get_token_from_maven_settings_xml()
    if token != "":
        return token

    token = getpass('Token: ')
    if token == "":
        print("Please provide the gitlab token ...")
        sys.exit(1)


def show_gui():
    all_projects, token, properties, gitlab_query_module = init()
    gui = BuildGui(all_projects, token, properties, gitlab_query_module)
    gui.build()
    # make sure this window pops up on top
    gui.root.lift()
    # gui.root.attributes("-topmost", True)
    # center window on screen
    gui.root.eval('tk::PlaceWindow %s center' % gui.root.winfo_toplevel())
    # show window
    gui.root.mainloop()


if __name__ == "__main__":
    show_gui()
